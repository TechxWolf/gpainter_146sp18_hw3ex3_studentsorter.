// Griffin Painter
// Spring 2018
// CSCI 146
// Homework 3, problem 3
import java.util.Arrays;

// begging of the class sortedStudentsApp
// this is hwere we will define how the information will be used via
// coding the methods of use
public class SortedStudentsApp {

    // replace this with an appropriate JavaDoc comment
    public static void main(String[] args) {

        // display welcome message
        System.out.println("The student Scores application\n");

        // get the number of students
        int studentCount = Console.getInt("Number of students: ",0,501);
        System.out.println();//print blank line
        
        // create the array
        Student[] students = new Student[studentCount];
        

        // get the data for each student
        for ( int i =0; i < students.length; i++){
            System.out.println("Student " + (i+1) );
            String lastName = Console.getString("Last name: ");
            String firstName = Console.getString("First name: ");
            int score = Console.getInt("Score: ", -1, 101);
            System.out.println();//blank line
            students[i] = new Student( lastName, firstName, score);
            
        }//end for loop

        // sort the array
        Arrays.sort( students ); //OK since we impemented the comparesTO
                                 // so that Java knows on what FIELDS to compare
                                 // the students in the array
        
        

        // print the array
        System.out.println("SUMMARY");
        for ( Student currentStudent : students) // for each student in our array
        {
            System.out.println(currentStudent.getSummaryLine());
            
        }//end enchanced for
    }
}//end class SortedStudentApp