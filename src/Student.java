// Griffin Painter
// Spring 2018
// CSCI 146
// Homework 3, problem 3
public class Student implements Comparable{

    /* FIELDS */
    private String lastName;
    private String firstName;
    private int score;

    /* CONSTRUCTORS */
    
    // constructor for the information needed for students
    // I.E. first and last names, and grade score
    public Student(String lastName, String firstName, int score) {
        this.lastName = lastName;
        this.firstName = firstName;
        this.score = score;
    }//end class student

    /* METHODS */
    
    // getter for lastName
    public String getLastName() {
        return this.lastName;
    }// end get method for lastName

    // getter for firstName 
    public String getFirstName() {
        return this.firstName;
    }//end get method for firstName

    // getter method for Score
    public int getScore() {
        return score;
    }// end get method for Score

    // getter for summary line, which is what we are going to display
    // with the information from all the getters we established above
    // this will be important to not mess up for readable output
    public String getSummaryLine() {
        return this.lastName + ", " + this.firstName
                + ": " + this.score;
    }// end get method for getSummaryLine

    // Override for the CompareTo method so we cann check students' names 
    // againnst one another for sorting purposes, such as in the GitBash example
    @Override
    public int compareTo ( Object o){
    
        Student S =(Student) o;
        
        // use the compareTo method as defined in the String class
        // to check to see if the first student's last name is less than,
        // equal to, or greater than the second student's last name
        int lastNameCompare = this.getLastName().compareTo( S.getLastName() );
        
        //what happens if the last names are the same
        if ( lastNameCompare !=0){// if they are different
            
            return lastNameCompare; // then reutn the value of computed above
        }else{// ... otherwise, if the last names are the same...
            // use the students' FIRST name for the comparison purpose
            return this.getFirstName().compareTo( S.getFirstName() );
            
        }//end if/else 
    }//end CONCRETE method compareTo
}//end class Student