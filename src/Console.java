// Griffin Painter
// Spring 2018
// CSCI 146
// Homework 3, problem 3
import java.util.Scanner;

// Start of the console, pre-written by authors for use
// NOTE: inn the workplace there wont be a pre-written Console and 
// it will have to be written or modified by me
public class Console {
    
    /* FIELDS */
    private static Scanner sc = new Scanner(System.in);

    /* METHODS */
    // This string is for our user to input the information for the students
    // sense it is a string it will prompt the user fot the students last name
    // and first name being changed based of which is being asked for at the 
    // time. It also removes any unecessary data
    public static String getString(String prompt) {
        System.out.print(prompt);
        String s = sc.next();  // read user entry
        sc.nextLine();  // discard any other data entered on the line
        return s;
    }// end get string method

    // This int is for the user to input the score of the student they have
    // identified above, this can be a double, but that is based off of what the
    // customer wants, it could be made more accurate. However we are using an
    // int in this case for ease of use
    public static int getInt(String prompt) {
        int i = 0;
        boolean isValid = false;
        while (!isValid) {
            System.out.print(prompt);
            if (sc.hasNextInt()) {
                i = sc.nextInt();
                isValid = true;
            } else {
                System.out.println("Error! Invalid integer. Try again.");
            }
            sc.nextLine();  // discard any other data entered on the line
        }
        return i;
    }// end static getInt

    // This is our parameters for our getInt method, they must have these. This
    // is used to set a max and min for our selected area. And it must have a 
    // string that prompts the user. String prompt is for clarity and the 
    // resrictions are so the user doesn't make any mistakes or affect the 
    // program in any way by user unwanted data
    public static int getInt(String prompt, int min, int max) {
        int i = 0;
        boolean isValid = false;
        while (!isValid) {
            i = getInt(prompt);
            if (i <= min) {
                System.out.println(
                        "Error! Number must be greater than " + min + ".");
            } else if (i >= max) {
                System.out.println(
                        "Error! Number must be less than " + max + ".");
            } else {
                isValid = true;
            }
        }
        return i;
    }//end static getInt Overloaded method

    // As stated in the getInt, since we are using an Int and not a double.
    // Though we could, we must check and make sure the user doesn't put in any
    // non valid value for the student score, if they are using a double
    public static double getDouble(String prompt) {
        double d = 0;
        boolean isValid = false;
        while (!isValid) {
            System.out.print(prompt);
            if (sc.hasNextDouble()) {
                d = sc.nextDouble();
                isValid = true;
            } else {
                System.out.println("Error! Invalid number. Try again.");
            }
            sc.nextLine();  // discard any other data entered on the line
        }
        return d;
    }//end get double method

    // This is our getDouble method that we will use to varify that the user
    // is inputting the proper innformation, as well as ourselves. We must have 
    // a prompt, a min double annd max double just like the int version.
    public static double getDouble(String prompt, double min, double max) {
        double d = 0;
        boolean isValid = false;
        while (!isValid) {
            d = getDouble(prompt);
            if (d <= min) {
                System.out.println(
                        "Error! Number must be greater than " + min + ".");
            } else if (d >= max) {
                System.out.println(
                        "Error! Number must be less than " + max + ".");
            } else {
                isValid = true;
            }
        }
        return d;
    }//end get double method, Overloaded
}//end class Console